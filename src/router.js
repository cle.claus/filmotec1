import Vue from 'vue';
import Router from 'vue-router';
import Accueil from './components/Accueil.vue';
import Films from './components/Films.vue';
import Search from './components/Search.vue';
import InfoFilm from './components/InfoFilm.vue';

Vue.use(Router);
export default new Router({
        routes: [
                { path: '/', name: 'accueil', component: Accueil },
                { path: '/films/:page', name: 'films', component: Films },
                { path: '/recherche', name: 'search', component: Search },
                { path: '/information/:id', name: 'info', component: InfoFilm }
        ],
        mode: 'history'
});